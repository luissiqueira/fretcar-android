package br.com.luissiqueira.fretcar.models;

/**
 * Created by luissiqueira on 18/02/14.
 */
public class Passagem {

    private String hora;
    private String chegada;
    private String tipo_onibus;
    private String numero_servico;
    private String valor;

    public String getChegada() {
        return chegada;
    }

    public void setChegada(String chegada) {
        this.chegada = chegada;
    }

    public String getVagas() {
        return vagas;
    }

    public void setVagas(String vagas) {
        this.vagas = vagas;
    }

    private String vagas;

    public Passagem(){
        this.hora = "17:30";
        this.tipo_onibus = "Convencional";
        this.valor = "R$ 16,00";
        this.vagas = "4";
    }

    public Passagem(String hora, String tipo_onibus, String valor, String vagas) {
        this.hora = hora;
        this.tipo_onibus = tipo_onibus;
        this.valor = valor;
        this.vagas = vagas;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = "R$ " + valor;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setNumero_servico(String numero_servico) {
        this.numero_servico = numero_servico;
    }

    public void setTipo_onibus(String tipo_onibus) {
        this.tipo_onibus = tipo_onibus;
    }

    public String getHora() {
        return hora;
    }

    public String getNumero_servico() {
        return numero_servico;
    }

    public String getTipo_onibus() {
        return tipo_onibus;
    }

}
