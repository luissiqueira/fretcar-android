package br.com.luissiqueira.fretcar.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import java.util.ArrayList;
import java.util.List;

import br.com.luissiqueira.fretcar.R;
import br.com.luissiqueira.fretcar.adapters.LocaisAdapter;
import br.com.luissiqueira.fretcar.models.Local;

/**
 * Created by luissiqueira on 18/02/14.
 */
public class LocaisFragment extends Fragment implements AdListener {

    public static LocaisFragment newInstance() {
        LocaisFragment fragment = new LocaisFragment();
        return fragment;
    }

    private AdView adView;




    public static List<Local> listLocais(){
        List<Local> nomes = new ArrayList<Local>();
        nomes.add(new Local("Camocim", "(88) 9765.7207"));
        nomes.add(new Local("Canindé", "(85) 9999.0111"));
        nomes.add(new Local("Deserto", "(88) 9665.3016 - (88) 8834.9563"));
        nomes.add(new Local("Fortaleza - Aeroporto", "(85) 3392.1917 - (85) 9950.3338 - "
                + "(85) 3392.1919 - aeroporto@openpoint.tur.br"));
        nomes.add(new Local("Fortaleza - Beira Mar", "(85) 3086.7055 - beachpointceara@gmail.com"));
        nomes.add(new Local("Fortaleza - Rodoviária João Tomé", "(85) 3402.2244 - (85) 9900.5671"));
        nomes.add(new Local("Jericoacoara", "(88) 9900.2109 - globalconnectionjericoacoara@gmail.com"));
        nomes.add(new Local("Piquet Carneiro", "(88) 9699.7148"));
        nomes.add(new Local("Preá", "(88) 9703.9695"));
        nomes.add(new Local("Quixadá", "(88) 3412.2424"));
        nomes.add(new Local("Quixeramobim", "(88) 3444.3620 - (88) 9624.7749"));
        nomes.add(new Local("Redenção", "(85) 9667.7749"));
        nomes.add(new Local("Santana", "(88) 9942.3495"));
        nomes.add(new Local("São Gonçalo", "(85) 8519.6800"));
        nomes.add(new Local("São Luís do Curu", "(85) 9683.0420"));
        nomes.add(new Local("Senador Pompeu", "(88) 9623.6048 - (88) 9633.0670 - (88) 9620.6888"));
        nomes.add(new Local("Sobral", "(88) 9920.7938"));
        nomes.add(new Local("Solonópole", "(88) 9977.5017"));
        nomes.add(new Local("Trairí", "(85) 9604.0208 - (85) 9179.9905"));
        nomes.add(new Local("Triângulo do Marco", "(88) 3664.4006 - (88) 3664.4047"));
        nomes.add(new Local("Uruburetama", "(85) 9985.2341"));

        return nomes;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_locais, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.listView);
        LocaisAdapter lAdapter = new LocaisAdapter(getActivity().getBaseContext(),
                                 R.layout.local_view, LocaisFragment.listLocais());
        listView.setAdapter(lAdapter);

        adView = new AdView(getActivity(), AdSize.BANNER, "a15305a9886c9b5");
        ((RelativeLayout)rootView.findViewById(R.id.relativeLayoutWrapper)).addView(adView);
        adView.loadAd(new AdRequest());
        AdView.LayoutParams par = (AdView.LayoutParams) adView.getLayoutParams();
        par.addRule(AdView.ALIGN_PARENT_BOTTOM);
        adView.setLayoutParams(par);
        adView.setAdListener(this);
        return rootView;
    }

    public int pxToDp(int px){
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (px * scale + 0.5f);
    }

    @Override
    public void onDestroy() {
        if(adView!=null) adView.destroy();
        super.onDestroy();
    }

    @Override
    public void onReceiveAd(Ad ad) {

    }

    @Override
    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {

    }

    @Override
    public void onPresentScreen(Ad ad) {

    }

    @Override
    public void onDismissScreen(Ad ad) {

    }

    @Override
    public void onLeaveApplication(Ad ad) {

    }
}
