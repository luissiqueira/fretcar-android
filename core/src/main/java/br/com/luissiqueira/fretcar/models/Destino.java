package br.com.luissiqueira.fretcar.models;

/**
 * Created by luissiqueira on 19/02/14.
 */
public class Destino {

    private String id, nome, destinos;

    public String getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getDestinos() {
        return destinos;
    }


    public Destino() {}

    public Destino(String id, String nome, String destinos) {
        this.id = id;
        this.nome = nome;
        this.destinos = destinos;
    }


}
