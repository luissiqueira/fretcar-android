package br.com.luissiqueira.fretcar.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.luissiqueira.fretcar.PassagensActivity;
import br.com.luissiqueira.fretcar.R;
import br.com.luissiqueira.fretcar.ViagemActivity;
import br.com.luissiqueira.fretcar.models.Passagem;


/**
 * Created by luissiqueira on 18/02/14.
 */
public class PassagensAdapter extends ArrayAdapter<Passagem>{

    PassagensActivity activity = null;
    Context c = null;
    public PassagensAdapter(Context context, int textViewResourceId){
        super(context, textViewResourceId);
    }

    public PassagensAdapter(Context context, int textViewResourceId, List<Passagem> itens){
        super(context, textViewResourceId, itens);
    }

    public PassagensAdapter(PassagensActivity p, Context context, int textViewResourceId, List<Passagem> itens){
        super(context, textViewResourceId, itens);
        this.activity = p;
        this.c = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.passagem_view, null);
        }
        final TextView tvVagas = ((TextView) v.findViewById(R.id.tvVagas));
        tvVagas.setText("Vagas: " + getItem(position).getVagas());
        final TextView tvHora = ((TextView) v.findViewById(R.id.tvHora));
        tvHora.setText("Saída: " + getItem(position).getHora());
        final TextView tvChegada = ((TextView) v.findViewById(R.id.tvChegada));
        tvChegada.setText("Previsão de Chegada: " + getItem(position).getChegada());
        final TextView tvValor = ((TextView) v.findViewById(R.id.tvValor));
        tvValor.setText(getItem(position).getValor());
        final TextView tvTipo = ((TextView) v.findViewById(R.id.tvTipo));
        tvTipo.setText("Ônibus " + getItem(position).getTipo_onibus());

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, ViagemActivity.class);
                i.putExtra("vagas", tvVagas.getText().toString());
                i.putExtra("hora", tvHora.getText().toString());
                i.putExtra("chegada", tvChegada.getText().toString());
                i.putExtra("valor", tvValor.getText().toString());
                i.putExtra("tipo", tvTipo.getText().toString());
                activity.startActivity(i);
            }
        });

        return v;

    }
}
