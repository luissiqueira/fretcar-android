package br.com.luissiqueira.fretcar.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import java.util.Date;
import java.text.SimpleDateFormat;

import br.com.luissiqueira.fretcar.PassagensActivity;
import br.com.luissiqueira.fretcar.R;
import br.com.luissiqueira.fretcar.SelecionarDestinoActivity;

/**
 * Created by luissiqueira on 18/02/14.
 */
public class ConsultaFragment extends Fragment {

    public static ConsultaFragment newInstance() {
        ConsultaFragment fragment = new ConsultaFragment();
        return fragment;
    }

    View v;
    static boolean saida_selecionada = false;
    static String saida_nome = null;
    static String destino_nome = null;
    private AdView adView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_consulta, container, false);

        rootView.findViewById(R.id.btnConsultar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dt = ((EditText) rootView.findViewById(R.id.etData)).getText().toString();
                if (saida_nome == null || destino_nome == null) {
                    Toast.makeText(getActivity().getBaseContext(), "Selecione o ponto de saída e o destino corretamente", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!validarData(dt)){
                    Toast.makeText(getActivity().getBaseContext(), "Preencha a data corretamente. Ex: 17/02/2014", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent i = new Intent(getActivity().getBaseContext(), PassagensActivity.class);
                i.putExtra("saida_nome", saida_nome);
                i.putExtra("destino_nome", destino_nome);
                i.putExtra("data", ((EditText) rootView.findViewById(R.id.etData)).getText().toString());
                startActivity(i);

            }
        });

        rootView.findViewById(R.id.tvSaida).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity().getBaseContext(), SelecionarDestinoActivity.class);
                saida_selecionada = true;
                startActivityForResult(i, 1);
            }
        });

        rootView.findViewById(R.id.tvDestino).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(saida_nome != null){
                    Intent i = new Intent(getActivity().getBaseContext(), SelecionarDestinoActivity.class);
                    i.putExtra("saida_nome", saida_nome);
                    saida_selecionada = false;
                    startActivityForResult(i, 1);
                }else{
                    Toast.makeText(getActivity().getBaseContext(), "Selecione o ponto de saída primeiro", Toast.LENGTH_SHORT).show();
                }
            }
        });

        v = rootView;

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        ((EditText) rootView.findViewById(R.id.etData)).setText(sdf.format(date));

        if(savedInstanceState != null){
            if(saida_nome != null)
                ((TextView)rootView.findViewById(R.id.tvSaida)).setText(saida_nome);
            if(destino_nome != null)
                ((TextView)rootView.findViewById(R.id.tvDestino)).setText(destino_nome);
        }

        AdRequest request = new AdRequest();
        request.addTestDevice("**");
        adView = new AdView(getActivity(), AdSize.BANNER, "a15305a9886c9b5");
        ((RelativeLayout)rootView.findViewById(R.id.relativeLayoutWrapper)).addView(adView);
        adView.loadAd(request);
        AdView.LayoutParams par = (AdView.LayoutParams) adView.getLayoutParams();
        par.addRule(AdView.ALIGN_PARENT_BOTTOM);
        adView.setLayoutParams(par);
        return rootView;
    }

    @Override
    public void onDestroy() {
        if(adView!=null) adView.destroy();
        super.onDestroy();
    }

    public boolean validarData(String data){
        try{
            if(data.length() != 10) return false;
            if(data.split("/")[0].length() != 2) return false;
            if(data.split("/")[1].length() != 2) return false;
            if(data.split("/")[2].length() != 4) return false;
            int dia = Integer.parseInt(data.split("/")[0]);
            int mes = Integer.parseInt(data.split("/")[1]);
            int ano = Integer.parseInt(data.split("/")[2]);
            if(dia < 1 || dia > 31) return false;
            if(mes < 1 || mes > 12) return false;
            if(ano < 2014 || ano > 2015) return false;
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode){
            case(1):
                if(saida_selecionada){
                    saida_nome = data.getStringExtra("result");
                    ((TextView)v.findViewById(R.id.tvSaida)).setText(saida_nome);
                    destino_nome = null;
                    ((TextView)v.findViewById(R.id.tvDestino)).setText("Toque para selecionar");
                }else{
                    destino_nome = data.getStringExtra("result");
                    ((TextView)v.findViewById(R.id.tvDestino)).setText(destino_nome);
                }
                break;
            default:
                break;
        }
    }
}
