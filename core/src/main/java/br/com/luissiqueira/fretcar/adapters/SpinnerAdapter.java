package br.com.luissiqueira.fretcar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.luissiqueira.fretcar.R;
import br.com.luissiqueira.fretcar.models.Destino;

/**
 * Created by luissiqueira on 19/02/14.
 */
public class SpinnerAdapter extends ArrayAdapter<Destino>{


    public SpinnerAdapter(Context context, int textViewResourceId){
        super(context, textViewResourceId);
    }

    public SpinnerAdapter(Context context, int textViewResourceId, List<Destino> itens){
        super(context, textViewResourceId, itens);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.spinner_row, null);
        }
        ((TextView)v.findViewById(R.id.tvNome)).setText(getItem(position).getNome());
        return v;
    }

}