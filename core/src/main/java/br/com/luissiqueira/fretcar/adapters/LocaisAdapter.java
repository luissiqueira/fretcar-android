package br.com.luissiqueira.fretcar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.luissiqueira.fretcar.R;
import br.com.luissiqueira.fretcar.models.Local;


/**
 * Created by luissiqueira on 18/02/14.
 */
public class LocaisAdapter extends ArrayAdapter<Local>{

    public LocaisAdapter(Context context, int textViewResourceId){
        super(context, textViewResourceId);
    }

    public LocaisAdapter(Context context, int textViewResourceId, List<Local> itens){
        super(context, textViewResourceId, itens);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.local_view, null);
        }
        ((TextView)v.findViewById(R.id.tvNome)).setText(getItem(position).getNome());
        ((TextView)v.findViewById(R.id.tvContato)).setText(getItem(position).getContato());
        return v;
    }
}
