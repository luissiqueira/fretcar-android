package br.com.luissiqueira.fretcar.models;

/**
 * Created by luissiqueira on 18/02/14.
 */
public class Local {

    private String nome;
    private String contato;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public Local(String nome, String contato) {
        this.nome = nome;
        this.contato = contato;
    }

    public Local(){}
}
