package br.com.luissiqueira.fretcar;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ListView;

import br.com.luissiqueira.fretcar.adapters.DestinosAdapter;
import br.com.luissiqueira.fretcar.util.ListDestinos;

/**
 * Created by luissiqueira on 19/02/14.
 */
public class SelecionarDestinoActivity extends ActionBarActivity {

    String saida_nome = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destino);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        saida_nome = getIntent().getStringExtra("saida_nome");

        ListView listView = (ListView) findViewById(R.id.listView);
        final DestinosAdapter lAdapter = new DestinosAdapter(this, getBaseContext(),
                R.layout.destino_view, saida_nome != null ?
                ListDestinos.getListDestinos(saida_nome) : ListDestinos.getList());
        listView.setAdapter(lAdapter);

        EditText search = (EditText) findViewById(R.id.search);
        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                lAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
    }

}