package br.com.luissiqueira.fretcar.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.luissiqueira.fretcar.models.Destino;

/**
 * Created by luissiqueira on 19/02/14.
 */
public class ListDestinos {

    private static List<Destino> fillList(){
        List<Destino> list = new ArrayList<Destino>();
        list.add(new Destino("57", "Acaraú", "2y153b191d1a011c141k1b3c1817161i111j351m1312"));
        list.add(new Destino("8", "Água Verde Baturité", "0a0b0v200c0g0e022901070x0k0f0h0l050t04060z0u0r0i22090j0d032n0y"));
        list.add(new Destino("32", "Água Verde Palmácia", "0v0c0o0q0p010s0x0n0m0t0z0r0y"));
        list.add(new Destino("106", "Almofala", "1l153b2x2w3a01141k3c2a2c17162o2z282j22111q351m13122n"));
        list.add(new Destino("41", "Amontada", "1l2y321n3b193a1d1p101a011c33141k1b3c181h341716361o1i111q1j351m3g1312"));
        list.add(new Destino("10", "Ant. Diogo", "080b0v0c0g0e022901070x0k0f0h0l050t04060z0u0r0i22090j0d032n0y"));
        list.add(new Destino("110", "Aracatiara", "151001331434111312"));
        list.add(new Destino("11", "Aracoiaba", "080a0v0c0g0e022901070x0k0f0h0l050t04060z0u0r0i22090d032n0y"));
        list.add(new Destino("59", "Aranaú", "15191a011418161o3512"));
        list.add(new Destino("31", "Aratuba", "080w0a0b0c0o0q0p0e022901070x0n0m050t04060z0u0r090d030y"));
        list.add(new Destino("67", "B. Vista PBA", "1x10011s1t1r1w"));
        list.add(new Destino("119", "Baia", "1l2y1501141k3c171611351m1312"));
        list.add(new Destino("105", "Baleia", "2y2w1001142a2c2o282j221113122n"));
        list.add(new Destino("83", "Banabuiu", "2m21012i2k272g2e2h2j24"));
        list.add(new Destino("72", "Barreira", "080201070504060903"));
        list.add(new Destino("104", "Barrento", "2y2x1001142a2c2o282j221113122n"));
        list.add(new Destino("128", "Barroso", "011s3i3j"));
        list.add(new Destino("12", "Baturité", "080w0a0b0v0o0q0p0g0e022901070x0f0h0n0m050t04060z0u0r0i22090d032n0y"));
        list.add(new Destino("45", "Bela Cruz", "1l151n1d1a011c141k1b1817161o1i1j353g12"));
        list.add(new Destino("24", "Boa Vista", "0w0v0c0q0p010s0x0n0m0t0z0r0y"));
        list.add(new Destino("26", "Buu", "0w0v0c0o0p010s0x0n0m0t0z0r0y"));
        list.add(new Destino("118", "C. da Volta", "2y1501141k16111q1m1312"));
        list.add(new Destino("25", "Cachoeira", "0w0v0c0o0q010s0x0n0m0t0z0r0y"));
        list.add(new Destino("16", "Caio Prado", "080a0b0c0e0201070f0h0504060i22090d032n"));
        list.add(new Destino("49", "Camocim", "1l15191a011c141k1b1816353g12"));
        list.add(new Destino("98", "Campos", "2p2s2j2r"));
        list.add(new Destino("69", "CANAÃ", "1v011u1s1t1r1w"));
        list.add(new Destino("94", "Cangati", "2b24"));
        list.add(new Destino("97", "Caninde", "2q2s2j2r"));
        list.add(new Destino("14", "Capristano", "080a0b0v0c0g0201070f0h0504060u0i22090d032n"));
        list.add(new Destino("61", "Carvoeiro", "15011416111q1312"));
        list.add(new Destino("2", "Ceasa", "080a0b0v200c0g0e2901070x0k0f0h0l050t04060z0u0r0i22090j0d032n0y"));
        list.add(new Destino("52", "Cemoaba", "1001111312"));
        list.add(new Destino("100", "Choró", "2q2p2j2r"));
        list.add(new Destino("36", "Croata", "15321v2x2w1g0133141h341t111w13121f"));
        list.add(new Destino("46", "Cruz", "1l151n191d011c141k1b1817161o1i1j353g12"));
        list.add(new Destino("81", "Cruz Gua", "080a0b0v0c020107050t04060903"));
        list.add(new Destino("73", "Dep. Irapuã Pinheiro", "2b012i2k2a272g2o2e2h2j2223242n"));
        list.add(new Destino("115", "Flecheiras", "011y1u1t1r1w"));
        list.add(new Destino("1", "Fortaleza", "1l080w2y150a320b1n0v1v3b2x2b202w3k0c190o0q3a0p0g1d1x0e1p021g101a2921370s1c1y071u0x2i330k140f1k1e1b0h2k3c2a0n0l3d0m18272c1h342505170t1z162g2o042e060z0u0r1s1t3i28262h1o0i2j22091i1r11231q3j240j0d35031m1w3g13121f2n0y2l"));
        list.add(new Destino("28", "G.do ferros", "0w0o0q0p010n0m0r"));
        list.add(new Destino("48", "Granja", "1l15191d1a01141k1b1816353g12"));
        list.add(new Destino("70", "Guagiru", "37011u1z1t1r1w"));
        list.add(new Destino("7", "Guaiúba", "080a0b0v200c0g0e0229010x0k0f0h0l050t04060z0u0r0i22090j0d032n0y"));
        list.add(new Destino("66", "Gualdrapas", "1x37011y1z1t1r1w"));
        list.add(new Destino("33", "Guaramiranga", "080w0a0b0v0c0o0q0p0201070n0m050t04060z0r09030y"));
        list.add(new Destino("90", "Ibaretama", "2b21012k2a272c252g2o2e28262h2j2223242n2l"));
        list.add(new Destino("101", "Ibicuitinga", "2v2u2j"));
        list.add(new Destino("111", "Icaraí", "153210011434111312"));
        list.add(new Destino("20", "Ideal", "080a0b0201070l050406090j03"));
        list.add(new Destino("40", "Itapipoca", "1l2y15321n3b2x2w193a1d1p101a011c331k1b3c181h341716361o1i111q1j351m3g1312"));
        list.add(new Destino("15", "Itapiúna", "080a0b0c0g0e0201070h0504060i22090d032n"));
        list.add(new Destino("56", "Itarema", "1l2y153b193a1d1a011c141b3c1817162z1i111q1j351m1312"));
        list.add(new Destino("50", "Jericoacoara", "011b"));
        list.add(new Destino("47", "Jijoca", "1l15191d1a011c141k1e1816353g12"));
        list.add(new Destino("17", "Jua", "080a0b0c0g0e0201070f0504060i22090d032n"));
        list.add(new Destino("92", "Juatama", "2b21012i272g2e2h2j24"));
        list.add(new Destino("120", "Juritianha", "1l2y153b01141k171611351m1312"));
        list.add(new Destino("82", "Km. Vinte", "2y2x2w21012i272c252g2o2e28262h2j2223243g2n2l"));
        list.add(new Destino("103", "Limoeiro do norte", "2t2u2j"));
        list.add(new Destino("23", "Lad. Grande", "0w0v0c0o0q0p010s0x0m0t0z0r0y"));
        list.add(new Destino("21", "Lag. São João", "080a0b0201070k0504060903"));
        list.add(new Destino("121", "Lagoinha", "011s1t1r1w"));
        list.add(new Destino("22", "Maranguape", "0w0v0c0o0q0p010s0x0n0t0z0r0y"));
        list.add(new Destino("44", "Marco", "1l151n191d1a011c141k1b17161o1i1j353g12"));
        list.add(new Destino("79", "Milhã", "2b21012i2k2a2g2o2e2h2j2223242n"));
        list.add(new Destino("84", "Minerolândia", "2y2x2w012i2a252g2o2e28262h2j22233g2n2l"));
        list.add(new Destino("53", "Miraima", "15100114111312"));
        list.add(new Destino("112", "Moitas", "153210013314111312"));
        list.add(new Destino("77", "Mombaca", "012i2a2c2g2o2e262h2j223g2n2l"));
        list.add(new Destino("5", "Monguba", "080a0b0v200c0g0e022901070x0k0f0h0l0t04060z0u0r0i22090j0d032n0y"));
        list.add(new Destino("102", "Morada nova", "2t2v2j"));
        list.add(new Destino("43", "Morrinhos", "1l2y153b191a01141k3c1816361i111j1m1312"));
        list.add(new Destino("29", "Mulungu", "080w0a0b0v0c0o0q0p022901070x0n0m0504060z0r09030y"));
        list.add(new Destino("71", "Mundaú", "011y1u1t1r1w"));
        list.add(new Destino("42", "Nascente", "1l2y151n3b193a1d1p1a011c141k1b3c1817361o1i111q1j351m1312"));
        list.add(new Destino("88", "Ocara", "2b21012i2k2a272c252o2e28262h2j2223242n2l"));
        list.add(new Destino("96", "Oiticica", "2y2x2w21012i2a272c252g2e28262h2j2223242n2l"));
        list.add(new Destino("4", "Olho D'agua", "080a0b0v200c0g0e022901070x0k0f0h0l050t060z0u0r0i22090j0d032n0y"));
        list.add(new Destino("107", "Oriente", "2y1k1q1m"));
        list.add(new Destino("86", "Pacajus", "2b21012i2k2a272c252g2o28262h2j2223242n2l"));
        list.add(new Destino("6", "Pacatuba", "080a0b0v200c0g0e022901070x0k0f0h0l050t040z0u0r0i22090j0d032n0y"));
        list.add(new Destino("35", "Pacoti", "080w0a0b0v0c0o0q0p0201070x0n0m050t04060r09030y"));
        list.add(new Destino("30", "Pai João", "080a0b0v0c0e020107050406090d03"));
        list.add(new Destino("27", "Palmácia", "080w0a0b0v0c0o0q0p02010s070x0n0m050t04060z09030y"));
        list.add(new Destino("64", "Paracuru", "1v3k1x013d1t3i1r3j1w"));
        list.add(new Destino("65", "Paraipaba", "1v1x1037011y1u3d1z1s1r1w"));
        list.add(new Destino("114", "Patriarca", "15011417161i1j"));
        list.add(new Destino("126", "Pecém", "3k011s3j"));
        list.add(new Destino("80", "Pedra branca", "2y2x2w012i2a2c2g2o2e2h2j22232n"));
        list.add(new Destino("78", "Piquet Carneiro", "012i2a2c252g2o2e2h2j223g2n2l"));
        list.add(new Destino("89", "Piranji", "2b21012i2k2a272c252g2o2e28262j2223242n2l"));
        list.add(new Destino("60", "Preá", "151n191a011418163512"));
        list.add(new Destino("18", "Quixadá", "080a0b0c0g0e0201070f0h05040622090d032n"));
        list.add(new Destino("91", "Quixadá via BR116", "2y2x2b2w2q2p2s21012i2t2k2a2v272c252u2g2o2e28262h2223242r3g2n2l"));
        list.add(new Destino("74", "Quixeramobim", "082y0a0b2x2w0c0g0e022101072i0f0h2a272c25052g2o042e0628262h0i2j0923240d033g2n2l"));
        list.add(new Destino("9", "Redenção", "080a0b0v200c0g0e022901070x0k0f0h0l050t04060z0u0r0i220j0d032n0y"));
        list.add(new Destino("54", "Santana", "1l15191a01141k18171636111j1312"));
        list.add(new Destino("63", "São Gonçalo", "1v1x37011y1u3d1z1s1t1w"));
        list.add(new Destino("37", "São Luís do Curu", "1l2y15323b2x2w3a1p1g100133141k3c1h3417161i1q351m13121f"));
        list.add(new Destino("75", "Senador Pompeu", "21012i2a272c2g2o2e282h2j22243g2n"));
        list.add(new Destino("62", "Sítio Alegre", "2y153a1p01141k162z111m1312"));
        list.add(new Destino("127", "Siupé", "3k011s3i"));
        list.add(new Destino("55", "Sobral", "1l15191a141k181716361i"));
        list.add(new Destino("76", "Solonópole", "2b2m21012i2k2a272g2o2e2h2j22232n"));
        list.add(new Destino("19", "Susto", "080a0201070k0504060903"));
        list.add(new Destino("13", "T. Aracoiaba", "080a0b0v0c0g0e0201070f0h0504060u0i2209032n"));
        list.add(new Destino("113", "T. Morrinhos", "1l2y151n3b191d1a011c141k1b3c18161o111m1312"));
        list.add(new Destino("99", "Targinos", "2q2p2s2j"));
        list.add(new Destino("3", "Timbó", "080a0b0v200c0g0e022901070x0k0f0h0l050t04060z0u0r0i22090j0d2n0y"));
        list.add(new Destino("58", "Torrões", "1l2y153b3a01141k3c17162z111q351312"));
        list.add(new Destino("68", "Trairi", "1v1x1037011y1u3d1z1s1t1r"));
        list.add(new Destino("124", "Transfer", "01"));
        list.add(new Destino("39", "Tururu", "1l2y15323b2x2w3a1p1g100133141k3c1h3417161i111q351m12"));
        list.add(new Destino("38", "Umirim", "1l2y15321n3b2x2w193a1d1p1g101a011c33141k1b3c181h3417161o1i111q351m131f"));
        list.add(new Destino("51", "Uruburetama", "10011112"));
        list.add(new Destino("95", "Uruque", "082y0a0b2x2w0c0g0e022101072i0f0h2a272c25052g2o042e0628262h0i2j220923240d032l"));
        list.add(new Destino("34", "Volta do Rio", "080w0a0b0v0c0o0q0p0201070x0n0m050t04060z0r0903"));
        list.add(new Destino("93", "Zorra", "012i2a2c252g2o2e262h2j222n"));
        return list;
    }

    public static List<Destino> getList(){
        List<Destino> list = fillList();

        Comparator<Destino> cmp = new Comparator<Destino>() {
            @Override
            public int compare(Destino destino, Destino destino2) {
                if(destino.getNome().equalsIgnoreCase("fortaleza")) return -1;
                if(destino2.getNome().equalsIgnoreCase("fortaleza")) return 1;
                return destino.getNome().compareTo(destino2.getNome());
            }
        };

        Collections.sort(list, cmp);

        return list;
    }

    public static String getID(String n){
        for(Destino d : getList())
            if (d.getNome().equals(n))
                return d.getId();

        return null;
    }

    public static List<Destino> getListDestinos(String n){

        List<Integer> list_ids = new ArrayList<Integer>();

        for(Destino d : getList())
            if (d.getNome().equals(n)) {
                String destinos = d.getDestinos();
                for (int i = 0; i < destinos.length(); i = i + 2) {
                    String des = destinos.substring(i, i + 2);
                    list_ids.add(Integer.parseInt(des, 36));
                }
                break;
            }

        List<Destino> list = new ArrayList<Destino>();

        for(Destino d : getList())
            if(list_ids.contains(Integer.parseInt(d.getId())))
                list.add(d);

        Comparator<Destino> cmp = new Comparator<Destino>() {
            @Override
            public int compare(Destino destino, Destino destino2) {
                if(destino.getNome().equalsIgnoreCase("fortaleza")) return -1;
                if(destino2.getNome().equalsIgnoreCase("fortaleza")) return 1;
                return destino.getNome().compareTo(destino2.getNome());
            }
        };

        Collections.sort(list, cmp);

        return list;
    }

}
