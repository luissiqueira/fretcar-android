package br.com.luissiqueira.fretcar.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.luissiqueira.fretcar.R;
import br.com.luissiqueira.fretcar.SelecionarDestinoActivity;
import br.com.luissiqueira.fretcar.models.Destino;
import br.com.luissiqueira.fretcar.util.ListDestinos;


/**
 * Created by luissiqueira on 18/02/14.
 */
public class DestinosAdapter extends ArrayAdapter<Destino>{

    SelecionarDestinoActivity activity = null;
    List<Destino> itens = null;

    public DestinosAdapter(Context context, int textViewResourceId){
        super(context, textViewResourceId);
    }

    public DestinosAdapter(Context context, int textViewResourceId, List<Destino> itens){
        super(context, textViewResourceId, itens);
        this.itens = itens;
    }

    public DestinosAdapter(SelecionarDestinoActivity a, Context context, int textViewResourceId, List<Destino> itens){
        super(context, textViewResourceId, itens);
        this.activity = a;
        this.itens = itens;
    }

    @Override
    public Destino getItem(int position) {
        return itens.get(position);
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.destino_view, null);
        }
        ((TextView)v.findViewById(R.id.tvNome)).setText(getItem(position).getNome());
        final int p = position;
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", ((TextView) view.findViewById(R.id.tvNome)).getText().toString());
                activity.setResult(1, returnIntent);
                activity.finish();
            }
        });
        return v;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itens = (List<Destino>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                List<Destino> FilteredArrayNames = new ArrayList<Destino>();

                constraint = constraint.toString().toLowerCase();
                for(Destino d : ListDestinos.getList()){
                    if(d.getNome().toLowerCase().startsWith(constraint.toString()))
                        FilteredArrayNames.add(d);
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;

                return results;
            }
        };

        return filter;
    }
}
