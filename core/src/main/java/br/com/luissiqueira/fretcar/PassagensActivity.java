package br.com.luissiqueira.fretcar;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import br.com.luissiqueira.fretcar.adapters.PassagensAdapter;
import br.com.luissiqueira.fretcar.models.Passagem;
import br.com.luissiqueira.fretcar.util.ListDestinos;

/**
 * Created by luissiqueira on 18/02/14.
 */
public class PassagensActivity extends ActionBarActivity {

    ProgressDialog pDialog;
    PassagensAdapter pAdapter;
    ListView listView;
    static List<Passagem> list;
    static String idSaida, idDestino, dtViagem;
    private AdView adView;
    private boolean erro = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passagens);

        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        listView = (ListView)findViewById(R.id.listView);

        String s = getIntent().getStringExtra("saida_nome");
        String d = getIntent().getStringExtra("destino_nome");
        String dt = getIntent().getStringExtra("data");
        ((TextView)findViewById(R.id.textView)).setText(s + " - " + d + " (" + dt.substring(0,5) + ")");
        if(savedInstanceState == null){
            idSaida = ListDestinos.getID(s);
            idDestino = ListDestinos.getID(d);
            dtViagem = dt.substring(8) + dt.substring(3,5) + dt.substring(0,2);
            new MyAsync().execute();
        }
        else{
            pAdapter = new PassagensAdapter(PassagensActivity.this, getBaseContext(), R.layout.passagem_view, list);
            listView.setAdapter(pAdapter);
        }

        AdRequest request = new AdRequest();
        request.addTestDevice("**");
        adView = new AdView(this, AdSize.BANNER, "a15305a9886c9b5");
        ((RelativeLayout)findViewById(R.id.relativeLayoutWrapper)).addView(adView);
        adView.loadAd(request);
        AdView.LayoutParams par = (AdView.LayoutParams) adView.getLayoutParams();
        par.addRule(AdView.ALIGN_PARENT_BOTTOM);
        adView.setLayoutParams(par);

    }

    @Override
    public void onDestroy() {
        if(adView!=null) adView.destroy();
        super.onDestroy();
    }

    class MyAsync extends AsyncTask<Void, Integer, String>
    {
        @Override
        protected void onPreExecute (){
            list = new ArrayList<Passagem>();
            pDialog = ProgressDialog.show(PassagensActivity.this, "",
                    "Carregando. Por favor aguarde...", true);
        }

        @Override
        protected String doInBackground(Void...arg0) {
            try {
                String url = "http://177.19.133.149/cgi-bin/br5.cgi?ida=soloida&txt_desde="+ idSaida +"&txt_hasta="+ idDestino+"&fecha="+dtViagem+"&fecha_vuelta="+dtViagem;
                HttpClient httpclient = new DefaultHttpClient(); // Create HTTP Client
                HttpGet httpget = new HttpGet(url); // Set the action you want to do
                HttpResponse response = null; // Executeit
                response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent(); // Create an InputStream with the response
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) // Read line by line
                    sb.append(line + "\n");
                String resString = sb.toString(); // Result is here
                is.close(); // Close the stream

                Document doc = Jsoup.parse(resString);
                Elements linhas = doc.select("form table tr");
                for(Element l : linhas){
                    Passagem p = new Passagem();
                    p.setHora(l.select("td").get(1).text());
                    p.setVagas(l.select("td").get(3).text());
                    p.setTipo_onibus(l.select("td").get(5).text().contains("CONV") ? "Convencional" : "Executivo");
                    p.setValor(l.select("td").get(4).text());
                    p.setChegada(l.select("td").get(2).text());
                    list.add(p);
                }
                if(list.size() > 0) list.remove(0);

            } catch (Exception e) {
                erro = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            if(list.size() == 0 && !erro)
                Toast.makeText(getBaseContext(), "Não foram encontrados horário com nessa rota.", Toast.LENGTH_SHORT).show();
            if(erro)
                Toast.makeText(getBaseContext(), "Verifique sua conexão com a internet e tente novamente.", Toast.LENGTH_SHORT).show();
            erro = false;
            pAdapter = new PassagensAdapter(PassagensActivity.this, getBaseContext(), R.layout.passagem_view, list);
            listView.setAdapter(pAdapter);
        }
    }



}
